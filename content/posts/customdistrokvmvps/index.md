---
title: "Installing custom distros on KVM VPS's (suicide method)"
date: 2020-06-22T16:45:00+03:00
author: "Stnby"
---

## Why would anyone do this?
Most VPS providers only give so many distros to choose from.
Usually all they provide is ancient **Debian** *Debilian* based distros and sometimes **CentOS** or **Fedora** if you're lucky and for *Ubuntu I have no words to say*. Might as well use **TempleOS** or **Haiku** :)

Real *chad sysadmins* need [Void (Linux)](https://voidlinux.org/) or at least that's what I prefer myself and that's what I'm going to use in this *tutorial*.

## What do we need to begin?
You need a VPS provider that gives **hardware console access** and any distro as a starting point and also *some brains obviously*.

{{< figure src="Screenshot_2020-06-21 Instances - OpenStack Dashboard.png" title="Instances - nazwa.pl" >}}
{{< figure src="Screenshot_2020-06-21 server362832 - OpenStack Dashboard.png" title="Instance Console - nazwa.pl" >}}

## Procedure

Download iso file to tmpfs because we can't read and write to disk at same time.
```sh
wget "https://alpha.de.repo.voidlinux.org/live/current/void-live-x86_64-20191109.iso" -O /dev/shm/void.iso
```

Remount all mounted filesystems in read-only mode to avoid corruption.
```sh
echo 'u' > /proc/sysrq-trigger
```

Flash iso to hard drive using `dd`.
```sh
dd if=/dev/shm/void.iso of=/dev/sda
```

Then **HARD REBOOT** because after *dd'ing* the drive the chances are low that `reboot` command will do anything.

{{< figure src="2020-06-21_18-09-34.png" title="Hard Reboot Instance" >}}

Open **Hardware Console** and in bootloader menu choose option to boot in RAM mode or modify the boot params *(Internet is your friend)*.
{{< figure src="image_2020-05-21_01-10-45.png" title="Boot using RAM mode" >}}

While in installer make sure to unmount the boot drive.
```
# lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0 318.5M  1 loop
loop1    7:1    0   801M  1 loop /run/rootfsbase
sda      8:0    0    25G  0 disk /run/initramfs/live
└─sda1   8:1    0    25G  0 part
# umount /dev/sda
``` 

## Installing Void (Linux) in BIOS mode on GPT partiton table

This paragraph exists because I ran into some issues with **GRUB2** while installing **Void (Linux)** using MBR method.
The issue is that GRUB2 doesn't fit in MBR sector anymore.

That's why we need to have a dedicated partition for **GRUB2**.

### Partitioning the disk

I'm using `parted` as an example how partitions should be created.
```
# parted /dev/sda
GNU Parted 3.3
Using /dev/sda
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) mklabel gpt
(parted) unit mib
(parted) mkpart primary 1 3
(parted) name 1 grub
(parted) set 1 bios_grub on
(parted) mkpart primary 3 -1
(parted) name 2 rootfs
(parted) print
Model: Virtio Block Device (virtblk)
Disk /dev/sda: 25600MiB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags:

Number  Start    End       Size      File system  Name    Flags
 1      1.00MiB  3.00MiB   2.00MiB                grub    bios_grub
 2      3.00MiB  25599MiB  25596MiB               rootfs
(parted) quit
```

### Running the `void-installer`
```
# void-installer
```
Oh that was hard wasn't it.

Do everything as you would normally just skip the **Partitioning** part.
{{< figure src="image_2020-05-27_20-18-12.png" >}}
