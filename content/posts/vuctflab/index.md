---
title: "Simple VU Capture The Flag lab"
date: 2020-05-27T03:14:00+03:00
author: "Stnby"
---

## Structure
This CTF consists of **3 VirtualBox image files**.

1. ~~[Skids_playground.ova](https://archive.org/download/simple-vu-capture-the-flag-lab/Skids_playground.ova)~~ - Beginner *(Skid)* level. **Actually not so easy nor interesting... Suggest skipping.*
2. [The_Young_God_lives_here.ova](https://archive.org/download/simple-vu-capture-the-flag-lab/The_Young_God_lives_here.ova) - Intermediate *(Elite gamer)* level.
3. [Slitaz_go_brr.ova](https://archive.org/download/simple-vu-capture-the-flag-lab/Slitaz_go_brr.ova) - Experienced *(1337 Haxor)* level.

To start playing you will need to install [VirtualBox](https://www.virtualbox.org/) and also the VM images listed above.

> **Note:** Login into the 1st image with `root:(blank)`

## Checking the flags
Flag format looks like this: `Flag0-SampleFlag`

To check the flag run:
```sh
curl "stnby.pl/ctf/flag0" -d "nick=TheITGuy" -d "flag=Flag0-SampleFlag"
```

You can also check previous flag submissions by other players:
```sh
curl "stnby.pl/ctf/level0.log"
```

And yes no *shiny/bloated* flag submission system. This is a CTF after all just learn how to use **curl** :)

## Screenshots
{{< figure src="VirtualBox_Skids_playground_27_05_2020_04_04_24.png" title="Skids playground" >}}
{{< figure src="VirtualBox_The_Young_God_lives_here_27_05_2020_01_22_51.png" title="The Young God lives here" >}}
{{< figure src="VirtualBox_Slitaz_go_brr_27_05_2020_00_26_11.png" title="Slitaz go brr" >}}
