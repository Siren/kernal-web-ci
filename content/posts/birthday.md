---
title: "1 year on telegram"
date: 2019-06-25T21:13:00+03:00
author: "Kernal"
---

Sorry comrades for making this post a little bit too late.
I wanted to set this up **yesterday (2019-06-24)** but oh well unexpected issues *like always*...
I know this doesn't mean *that* much to newer members of **kernal** but it really does matter for me. :)

Thank you all for keeping this community alive for so long.

So here I introduce **Kernal** *small / funny /* **crazy** community of hackers??? all around the world.
This will be the place where I and my team will try to push some *hopefully* useful life hacks / tips etc...

If you are interested in what we do or want to become a part of this feel free to join our [telegram group](https://t.me/kernals).

```
           -=(o '.
  Kernal      '.-.\
  Community   /|  \\
  2017-2019   '|  ||
              _\_):,_
```

> Keep in mind I'm not a writer...\
> Also I will try to not miss the actual birthday this time XD
