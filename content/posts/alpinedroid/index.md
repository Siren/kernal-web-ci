---
title: "Alpine chroot jail on Android"
date: 2020-08-20T01:31:00+03:00
author: "Stnby"
---

## But why?
You might ask what's the point of this because [Termux](https://termux.com/) exists? *Right?*\
Well the answer is simple...
 + **Why thfq not???** Show your *{boy,girl}friend* that you're a real *boomer*.
 + Termux is for *skids* with questionable skills and ~~nationalities~~. :-D
 + You also get full software and hardware control.
 + Way more efficient and extensible.

## Step #1 Root your device
Blah blah blah.\
Seriously this ain't a guide for this.\
But if you're really this green you can find some information in these great forums:
 + [XDA Developers](https://forum.xda-developers.com/) — Probably the most famous international Android forum.
 + [4PDA](https://4pda.ru/forum/) — Really nice forum that has all sorts of weird stuff. *(sadly it's in Russian but pretty easy to translate anyway)*.

## Step #2 Clone the Git repository
```sh
git clone 'https://gitlab.com/stnby/alpinedroid.git'
cd alpinedroid
```

## Step #3 Connect your phone over ADB
```
$ adb connect 10.0.1.2
connected to 10.0.1.2:5555
```

Click `Allow` button.

{{< figure src="Screenshot_20200818-230657_System_UI.jpg" title="\"Allow USB debugging\" prompt" >}}

## Step #4 Move over and execute the setup script
Transfer the `setup.sh` script.
```
$ adb push setup.sh /sdcard
setup.sh: 1 file pushed, 0 skipped. 0.1 MB/s (1310 bytes in 0.019s)
```

Execute the transferred script as **root**.
```
$ adb shell su -c 'sh /sdcard/setup.sh'
> prepare
< download rootfs
Connecting to dl-cdn.alpinelinux.org (151.101.194.133:80)
saving to 'rootfs.tar.gz'
rootfs.tar.gz    100% |********************************| 2554k  0:00:00 ETA
'rootfs.tar.gz' saved
> download rootfs
< extract rootfs
> extract rootfs
> configure
< configure
```
Setup is finished!!!

## Usage guide

Spawn an adb shell
```
$ adb shell
```

Within an adb shell run these commands to start the chroot environment.
```
beryllium:/ $ su
beryllium:/ # /data/alpinedroid/up.sh
beryllium:/ # /data/alpinedroid/chroot.sh
localhost:/#
```

Done we are now inside the alpine chroot environment.
```
localhost:/# apk add figlet
fetch http://dl-cdn.alpinelinux.org/alpine/v3.12/main/aarch64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.12/community/aarch64/APKINDEX.tar.gz
(1/1) Installing figlet (2.2.5-r1)
Executing busybox-1.31.1-r16.trigger
OK: 6 MiB in 15 packages
localhost:/# figlet -f small Kernal.eu
 _  __                  _
| |/ /___ _ _ _ _  __ _| |  ___ _  _
| ' </ -_) '_| ' \/ _` | |_/ -_) || |
|_|\_\___|_| |_||_\__,_|_(_)___|\_,_|

localhost:/# exit
```

Unmount the virtual partitions afterward by running this script.
```
beryllium:/ # /data/alpinedroid/down.sh
```
> **Note:** In case you want to remove the chroot environment make sure
> `/data/alpinedroid/mnt/sdcard` is unmounted!!!
> Otherwise *rip your data* :-D
