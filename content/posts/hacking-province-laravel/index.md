---
title: "Hacking a province in Turkey through Laravel"
date: 2021-03-14T03:34:00+03:00
author: "Siren"
---

{{< figure src="sirnak.png" title="This lovely province in eastern Turkey." >}}

I have attempted to email their IT staff and the province itself. Unfortunately the email address they provide on their contacts page is a Yandex free plan and they have ran out of storage.

```
<bilgi@sirnak.bel.tr>: host mx.yandex.net[2a02:6b8::311] said: 552 5.2.2
    Mailbox size limit exceeded 1615727140-JVz3btrGZk-5e9O9Bd0 (in reply to end of DATA command)
```

It's been weeks and I am yet to receive a response from their IT guy. I will be omitting some steps so they can not get hacked as easily :^) Let's get to it now.

## Laravel

> There's no point in looking into Laravel. Try something else.

said Stnby. Then I saw this.

{{< figure src="laravel.png" title="Laravel exception handler" >}}

We were able to log into phpMyAdmin using the leaked variables.

## phpMyAdmin

Seems like somebody is trying to save money.

{{< figure src="phpmyadmin.png" title="Is this even legal?" >}}

Alongside the local government's website and its e-gov services they're hosting several shopping websites, a news website and a school portal.

## No name Turkish CMS

Later on we logged ourselves in to their CMS.

{{< figure src="panel.png" >}}

We could edit all the content and upload files.

{{< figure src="edit.png" >}}

We did not go any further and decided to report it.

## Conclusion

We randomly stumbled upon this website and it took us less than 5 minutes to hack it. I don't know whether if it is that they don't have enough budget (but they should), or that budget is going somewhere else it shouldn't. Hopefully they will start caring more about security in the future.
